<?php

declare(strict_types=1);

namespace App\Tests\Provider;

use App\Provider\BNPProvider;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

final class BNPProviderTest extends TestCase
{
    function test_returns_array_of_loans_with_correct_keys_and_values()
    {
        $provider = new BNPProvider();
        $loans = $provider->getLoans();

        $this->assertIsArray($loans);
        $this->assertNotEmpty($loans);

        $this->assertArrayHasKey('bank', $loans[0]);
        $this->assertEquals('BNP', $loans[0]['bank']);
        $this->assertArrayHasKey('amount', $loans[0]);
        $this->assertIsNumeric($loans[0]['amount']);
        $this->assertArrayHasKey('duration', $loans[0]);
        $this->assertIsNumeric($loans[0]['duration']);
        $this->assertArrayHasKey('rate', $loans[0]);
        $this->assertIsNumeric($loans[0]['rate']);
    }
}
