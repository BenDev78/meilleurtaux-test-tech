<?php

declare(strict_types=1);

namespace App\Tests\Service;

use App\Provider\CarrefourProvider;
use App\Provider\SGProvider;
use App\Service\ComputeLoanComparisonService;
use PHPUnit\Framework\TestCase;

final class ComputeLoanComparisonServiceTest extends TestCase
{
    function test_valid_array_and_limit() {
        $data = [
            'amount' => 500000,
            'duration' => 20
        ];
        $limit = 5;

        $loanProviders = [
            [
                new CarrefourProvider(),
                new SGProvider()
            ]
        ];

        $service = new ComputeLoanComparisonService($loanProviders);
        $result = $service->compute($data, $limit);

        $this->assertCount(2, $result);
        $this->assertEquals('Carrefour', $result[0]->getBankName());
        $this->assertEquals('SG', $result[1]->getBankName());
    }
}
