### README pour lancer une application Symfony

## Prérequis
- PHP 8.2 ou supérieur
- Composer
- Symfony CLI

## Étapes pour lancer l'application
1. **Cloner le dépôt**
   - Ouvrez un terminal et exécutez la commande suivante pour cloner le dépôt :
     ```
     git clone https://gitlab.com/BenDev78/meilleurtaux-test-tech.git
     ```

2. **Installer les dépendances**
   - Naviguez dans le dossier du projet cloné et exécutez :
     ```
     composer install
     ```


4. **Lancer le serveur**
   - Utilisez la commande Symfony CLI pour démarrer le serveur :
     ```
     symfony server:start
     ```
   - Votre application devrait maintenant être accessible à l'adresse [http://localhost:8000](http://localhost:8000).

## Requêtes CURL

### Requête 1
```bash
curl -X POST http://localhost:8000/api/compute \
-H "Content-Type: application/json" \
-d '{"amount": 50000, "duration": 15, "email": "example1@example.com", "lastName": "Doe", "phone": "0123456789"}'
```

### Requête 2
```bash
curl -X POST http://localhost:8000/api/compute \
-H "Content-Type: application/json" \
-d '{"amount": 50000, "duration": 20, "email": "example2@example.com", "lastName": "Smith", "phone": "0987654321"}'
```

### Requête 3
```bash
curl -X POST http://localhost:8000/api/compute \
-H "Content-Type: application/json" \
-d '{"amount": 200000, "duration": 15, "email": "example3@example.com", "lastName": "Johnson", "phone": "0122334455"}'
```

### Requête 4
```bash
curl -X POST http://localhost:8000/api/compute \
-H "Content-Type: application/json" \
-d '{"amount": 100000, "duration": 25, "email": "example4@example.com", "lastName": "Williams", "phone": "0566778899"}'
```

### Requête 5
```bash
curl -X POST http://localhost:8000/api/compute \
-H "Content-Type: application/json" \
-d '{"amount": 50000, "duration": 15, "email": "example5@example.com", "lastName": "Brown", "phone": "0677889900"}'
```
