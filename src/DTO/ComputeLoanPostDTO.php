<?php

declare(strict_types=1);

namespace App\DTO;


use Symfony\Component\Validator\Constraints as Assert;

final class ComputeLoanPostDTO
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Positive]
        public readonly int $duration,

        #[Assert\NotBlank]
        #[Assert\Positive]
        public readonly int $amount,

        #[Assert\NotBlank]
        public readonly string $lastName,

        #[Assert\NotBlank]
        #[Assert\Email]
        public readonly string $email,

        #[Assert\NotBlank]
        public readonly string $phone,
    ) {
    }
}
