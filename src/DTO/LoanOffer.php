<?php

declare(strict_types=1);

namespace App\DTO;

final class LoanOffer
{
    private float $totalCost;

    public function __construct(
        private readonly string $bankName,
        private readonly int $amount,
        private readonly int $duration,
        private readonly float $rate
    )
    {
        $this->calculateTotalCost();
    }

    private function calculateTotalCost(): void
    {
        $this->totalCost = $this->amount * (1 + ($this->rate * $this->duration / 100));
    }

    // Getters here...
    public function getBankName(): string
    {
        return $this->bankName;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getDuration(): int
    {
        return $this->duration;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function getTotalCost(): float
    {
        return $this->totalCost;
    }
}
