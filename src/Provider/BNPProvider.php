<?php

declare(strict_types=1);

namespace App\Provider;

final class BNPProvider implements ProviderInterface
{
    private $data;

    public function __construct()
    {
        $this->data = json_decode(file_get_contents(__DIR__.'/../Data/BNP.json'), true);
    }

    public function getLoans(): array
    {
        return array_map(function ($entry) {
            return [
                'bank' => 'BNP',
                'amount' => $entry['montant'],
                'duration' => $entry['duree'],
                'rate' => $entry['taux'],
            ];
        }, $this->data);
    }
}
