<?php

declare(strict_types=1);

namespace App\Provider;

final class CarrefourProvider implements ProviderInterface
{
    private $data;

    public function __construct()
    {
        $this->data = json_decode(file_get_contents(__DIR__.'/../Data/CARREFOURBANK.json'), true);
    }

    public function getLoans(): array
    {
        return array_map(function ($entry) {
            return [
                'bank' => 'Carrefour',
                'amount' => $entry['montant_pret'],
                'duration' => $entry['duree_pret'],
                'rate' => $entry['taux_pret'],
            ];
        }, $this->data);
    }
}
