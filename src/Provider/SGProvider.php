<?php

declare(strict_types=1);

namespace App\Provider;

final class SGProvider implements ProviderInterface
{
    private array $data;

    public function __construct()
    {
        $this->data = json_decode(file_get_contents(__DIR__.'/../Data/SG.json'), true);
    }

    public function getLoans(): array
    {
        return array_map(function ($entry) {
            return [
                'bank' => 'SG',
                'amount' => $entry['amount'],
                'duration' => $entry['duration'],
                'rate' => $entry['rate'],
            ];
        }, $this->data);
    }
}
