<?php

namespace App\Controller;

use App\DTO\ComputeLoanPostDTO;
use App\Service\ComputeLoanComparisonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

class CompareController extends AbstractController
{
    #[Route('/api/compare', name: 'app_compare', methods: [Request::METHOD_POST])]
    public function index(
        #[MapRequestPayload] ComputeLoanPostDTO $dto,
        ComputeLoanComparisonService $comparisonService
    ): Response
    {
        $result = $comparisonService->compute(
            [
                'amount' => $dto->amount,
                'duration' => $dto->duration,
                'lastName' => $dto->lastName,
                'email' => $dto->email,
                'phone' => $dto->phone
            ]
        );

        return $this->json($result);
    }
}
