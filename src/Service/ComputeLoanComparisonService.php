<?php

declare(strict_types=1);

namespace App\Service;

use App\DTO\LoanOffer;

final class ComputeLoanComparisonService
{
    private array $loanProviders = [];

    public function __construct(private readonly iterable $providers)
    {
        foreach ($this->providers as $providers) {
            foreach ($providers as $provider) {
                $this->loanProviders[] = $provider;
            }
        }
    }

    public function compute(array $data = [], int $limit = 100): array
    {
        $offers = [];

        foreach ($this->loanProviders as $provider) {
            foreach ($provider->getLoans() as $loan) {
                if ($loan['amount'] === (int) $data['amount'] && $loan['duration'] === (int) $data['duration']) {
                    $offers[] = new LoanOffer($loan['bank'], $loan['amount'], $loan['duration'], $loan['rate']);
                }
            }
        }

        usort($offers, function ($a, $b) {
            return $a->getTotalCost() <=> $b->getTotalCost();
        });

        return array_slice($offers, 0, $limit);
    }
}
